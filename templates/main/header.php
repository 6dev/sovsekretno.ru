<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="google-site-verification" content="MxTJMmFfDBZbuU5pl5Ibw7hoIw-DyFTBeiZVtcUn5wA"/>
    <meta property="og:title" content="<?= $APPLICATION->ShowTitle(); ?>"/>
    <meta property="og:url" content="https://<?= SITE_SERVER_NAME . $APPLICATION->GetCurPage() ?>"/>
    <? $APPLICATION->ShowViewContent('og:image'); ?>

    <meta name="robots" content="all">
    <title><?= $APPLICATION->Showtitle(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="cmsmagazine" content="b6eb33e544106e46a6f19715cd585c8e"/>


    <meta name="yandex-verification" content="4aeec645501e4bbf"/>
    <meta name="yandex-verification" content="5cd194e8ac2905e4"/>
    <link href='/public/site/css/main2.css?t=1' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <!-- link href='/public/site/css/jquery-ui.css?t=1' rel='stylesheet' type='text/css' -->
    <!-- link href='/public/site/css/jquery.pnotify.css?t=1' rel='stylesheet' type='text/css' -->
    <!-- link href='/public/site/css/jquery.fancybox-1.3.4.css?t=1' rel='stylesheet' type='text/css' -->
    <!-- link href='/public/site/css/main2.css?t=1' rel='stylesheet' type='text/css' -->
    <!-- link href='/public/site/css/print.css?t=1' rel='stylesheet' type='text/css' -->
    <!--[if lte IE 8]>
    <link href='/public/site/css/ie.css?t=1' rel='stylesheet' type='text/css'>
    <![endif]-->

    <!-- JS -->
    <script type="text/javascript">
        var root_url = '/';
        var ctrlName = 'content';
    </script>

    <script src='https://yastatic.net/jquery/3.3.1/jquery.min.js' type='text/javascript'></script>
    <? /*
    <script src='/public/zf/js/jquery.js?239100' type='text/javascript'></script>
	<script src='/public/site/js/jquery.touchSwipe.min.js?239010' type='text/javascript'></script>
	<script src='/public/site/js/jquery.carouFredSel-6.1.0.js?239100' type='text/javascript'></script>
	<script src='/public/site/js/jquery.pnotify.js?239010' type='text/javascript'></script>
	<script src='/public/site/js/iflabel.js?239100' type='text/javascript'></script>
	<script src='/public/site/js/common.js?239100' type='text/javascript'></script>
	<script src='/public/site/js/jquery.fancybox-1.3.4.js?239010' type='text/javascript'></script>
	<script src='/public/site/js/polls.js?239010' type='text/javascript'></script>
	<script src='/public/site/js/adfox.asyn.code.ver3.js?239010' type='text/javascript'></script>
	<script src='/public/site/js/adfox.asyn.code.scroll.js?239010' type='text/javascript'></script>
	<script src='/public/zf/js/form.js?239010' type='text/javascript'></script>
    */
    require_once $_SERVER['DOCUMENT_ROOT'] . "/public/site/Mobile_Detect.php";
    $detect = new Mobile_Detect;
    ?>
    <link href="/favicon.ico" rel="icon" type="image/x-icon">
    <link href="/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="/favicon.ico" rel="shortcut">


    <script src="/public/site/js/jquery.cookie.min.js"></script>
    <? /*        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-2006571538605161",
            enable_page_level_ads: true
        });
    </script> */ ?>
</head>

<? $APPLICATION->ShowHead(); ?>

<body>

<?= $APPLICATION->ShowPanel(); ?>

<div class="wrapper wide">
    <? if (!($detect->isMobile() && !$detect->isTablet())) : ?>
        <div id="fb-root"></div>
        <script>(function (d, s, id, appid) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1&appId=" + appid;
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk', 464463903595861));
        </script>

        <script type="text/javascript" src="//vk.com/js/api/openapi.js?69"></script>
        <script type="text/javascript">
            VK.init({
                apiId: 3379504
            });
        </script>
    <? endif; ?>
    <div class="wrapper_inner">
        <div class="print-logo">
            <div class="innercont">
                <img src="/public/site/img/print-logo.jpg">
            </div>

        </div>
        <div class="header">

            <div class="clear"></div>


            <div class="innercont" style="margin-top:10px;">
                <!-- google search script -->

                <script>
                    (function () {
                        var cx = '009269743704177284058:cooegcbd1tk';
                        var gcse = document.createElement('script');
                        gcse.type = 'text/javascript';
                        gcse.async = true;
                        gcse.src = (document.location.protocol == 'https:' ? 'https:' : '') +
                            '//cse.google.com/cse.js?cx=' + cx;
                        var s = document.getElementsByTagName('script')[0];
                        s.parentNode.insertBefore(gcse, s);
                    })();
                </script>


                <div class="topHeader_search">
                    <form name="search" action="/search/" method="get">
                        <div class="search_box">
                            <input class="input_text" placeholder="Поиск по сайту" name="q" type="text"/>
                            <input class="button" type="submit" value=""/>
                        </div>
                    </form>

                </div>


                <div class="social_button" style="float:left;">


                    <ul>
                        <li>
                            <a href="https://www.facebook.com/SovSekretno.ru" class="facebook">
                                <span></span>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/SovSekretno" class="twitter">
                                <span></span>
                            </a>
                        </li>
                        <li>
                            <a href="//ok.ru/group/53052455649345"><img async
                                                                        src="//habrastorage.org/getpro/habr/company/367/a15/296/367a15296c82d14ffdcd174ea6720928.png"
                                                                        width="15" hspace="3"></a>
                            <span></span>
                            </a>
                        </li>
                        <li>
                            <a href="//vk.com/sov.sekretno" class="vkontakte">
                                <span></span>
                            </a>
                        </li>

                        <li>
                            <a href="/rss/all/" class="rss">
                                <span></span>
                            </a>
                        </li>
                    </ul>
                </div>


                <a href="/">
                    <div class="logotype">
                        <img src="/public/site/img/new_images/logov.jpeg" alt="">

                </a>


            </div>


            <div class="toggle_mnu">
            <span class="sandwich">
            <span class="sw-topper"></span>
            <span class="sw-bottom"></span>
            <span class="sw-footer"></span>
            </span>
            </div>
            <div class="clear"></div>
            <ul class="top_menu block">


                <? $APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
                        "ROOT_MENU_TYPE" => "top",
                        "MAX_LEVEL" => "2",
                        "CHILD_MENU_TYPE" => "section",
                        "USE_EXT" => "Y",
                        "DELAY" => "N",
                        "ALLOW_MULTI_SELECT" => "Y",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "36000",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => ""
                    )
                ); ?>


            </ul>
            <div class="clear"></div>

            <?
            //if ($USER->getID() == 24) { ?>
            <!-- Yandex.RTB R-A-423943-4 -->
            <div id="yandex_rtb_R-A-423943-4" style="margin-top: 10px;"></div>
            <script type="text/javascript">
                (function (w, d, n, s, t) {
                    w[n] = w[n] || [];
                    w[n].push(function () {
                        Ya.Context.AdvManager.render({
                            blockId: "R-A-423943-4",
                            renderTo: "yandex_rtb_R-A-423943-4",
                            async: true
                        });
                    });
                    t = d.getElementsByTagName("script")[0];
                    s = d.createElement("script");
                    s.type = "text/javascript";
                    s.src = "//an.yandex.ru/system/context.js";
                    s.async = true;
                    t.parentNode.insertBefore(s, t);
                })(this, this.document, "yandexContextAsyncCallbacks");
            </script>
            <? //} ?>
        </div>
    </div>
    <div class="clear"></div>
    <div class="content">
        <div class="innercont with_spec">
            <!-- table class="main_specs floatcontainer" style="overflow: visible;">
    <tr>
                                    <td>
                <a href="/specials/id/6/">Колонка редактора<div class="materials">72 материала</div></a>
            </td>
                                    <td>
                <a href="/specials/id/7/">Секреты Украины<div class="materials">43 материала</div></a>
            </td>
                                    <td>
                <a href="/specials/id/15/">ПОЛКОВОДЦЫ: Двойной портрет на фоне битвы<div class="materials">9 материалов</div></a>
            </td>
                                    <td>
                <a href="/specials/id/18/">Страна за рулем<div class="materials">15 материалов</div></a>
            </td>
                                    <td>
                <a href="/specials/id/13/">Война санкций<div class="materials">21 материал</div></a>
            </td>
                                    <td>
                <a href="/specials/id/12/">Секретные технологии<div class="materials">38 материалов</div></a>
            </td>
                            </tr>
</table -->
            <div class="left_blocks"></div>

            <div class="right_blocks">
                <div id="c_right">
                    <? if (!($detect->isMobile() && !$detect->isTablet())) : ?>
                        <? if ($USER->IsAdmin()) {
                            echo "<div class='polls'><h3>ОПРОСЫ</h3><div class='poll'>";
                            $APPLICATION->IncludeComponent("bitrix:voting.current", ".main_poll", Array(
                                "AJAX_MODE" => "Y",    // Включить режим AJAX
                                "AJAX_OPTION_ADDITIONAL" => "",    // Дополнительный идентификатор
                                "AJAX_OPTION_HISTORY" => "N",    // Включить эмуляцию навигации браузера
                                "AJAX_OPTION_JUMP" => "N",    // Включить прокрутку к началу компонента
                                "AJAX_OPTION_STYLE" => "Y",    // Включить подгрузку стилей
                                "CACHE_TIME" => "3600",    // Время кеширования (сек.)
                                "CACHE_TYPE" => "A",    // Тип кеширования
                                "CHANNEL_SID" => "MAIN",    // Группа опросов
                                "VOTE_ALL_RESULTS" => "N",    // Показывать варианты ответов для полей типа Text и Textarea
                                "VOTE_ID" => "",    // ID опроса
                                "COMPONENT_TEMPLATE" => ".userfield"
                            ),
                                false
                            );
                            echo "</div></div>";
                        }
                        ?>


                        <!-- mt.sovsek
                    <div id="unit_84666"><a href="//advert.mirtesen.ru/">??????? advert.mirtesen.ru</a></div>

                    <script type="text/javascript" charset="utf-8">
                    (function() {
                    var sc = document.createElement('script'); sc.type = 'text/javascript'; sc.async = true;
                    sc.src = '//js.advert.mirtesen.ru/data/js/84666.js'; sc.charset = 'utf-8';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sc, s);
                    }());
                    </script>

                        -->
                        <!-- <div class="banners-right">
                            <div class="banners_inner">
                                <div class="bann_title">Новости СМИ2</div>
                             <div id="smi2adblock_74721"></div>

                            <script type="text/javascript" charset="utf-8">
                              (function() {
                              var sc = document.createElement('script'); sc.type = 'text/javascript'; sc.async = true;
                              sc.src = '//js.smi2.ru/data/js/74721.js'; sc.charset = 'utf-8';
                              var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sc, s);
                              }());
                            </script>

                        </div>
                        </div> -->


                        <div class="face_book">
                            <div class="fb-like-box" data-href="//www.facebook.com/SovSekretno.ru" data-width="246"
                                 data-color="ed1c24"
                                 data-height="226" data-show-faces="true" data-stream="false" data-header="true"></div>
                        </div>

                        <div id="ok_group_widget"></div>

                        <script>
                            !function (d, id, did, st) {
                                var js = d.createElement("script");
                                js.src = "https://connect.ok.ru/connect.js";
                                js.onload = js.onreadystatechange = function () {
                                    if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
                                        if (!this.executed) {
                                            this.executed = true;
                                            setTimeout(function () {
                                                OK.CONNECT.insertGroupWidget(id, did, st);
                                            }, 0);
                                        }
                                    }
                                }
                                d.documentElement.appendChild(js);
                            }(document, "ok_group_widget", "53052455649345", "{width:250,height:300}");
                        </script>


                        <div id="vk_groups_side"></div>
                        <script type="text/javascript">
                            VK.Widgets.Group("vk_groups_side", {
                                mode: 0,
                                width: "246",
                                height: "220",
                                color1: 'FFFFFF',
                                color2: 'ed1c24',
                                color3: 'ed1c24'
                            }, 45929147);
                        </script>

                        <div class="top5 top10">
                            <h2>ТОП</h2>
                            <h3>Самое популярное</h3>
                            <div class="inner">
                                <ul>
                                    <?
                                    CModule::IncludeModule('iblock');
                                    $arSelect = Array("DETAIL_PAGE_URL", "ID", "NAME", "DATE_ACTIVE_FROM", "SHOW_COUNTER");
                                    $arFilter = Array("IBLOCK_ID" => 3, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
                                    $res = CIBlockElement::GetList(Array("SHOW_COUNTER" => "DESC"), $arFilter, false, Array("nPageSize" => 5), $arSelect);
                                    while ($ob = $res->GetNextElement()) {
                                        $arFields = $ob->GetFields();

                                        ?>


                                        <li>
                                            <a href="<?= $arFields[DETAIL_PAGE_URL]; ?>"><?= $arFields[NAME]; ?></a>
                                            <div class="floatcontainer post_controls">
                                                <div class="comments" data-xid="articles_6208">0</div>
                                                <div class="viewings"><?= $arFields[SHOW_COUNTER]; ?></div>
                                            </div>
                                        </li>

                                    <? } ?>


                                </ul>
                            </div>
                        </div>


                        <div class="banners-right">
                            <!-- Yandex.RTB R-A-423943-2 -->
                            <div id="yandex_rtb_R-A-423943-2"></div>
                            <script type="text/javascript">
                                (function (w, d, n, s, t) {
                                    w[n] = w[n] || [];
                                    w[n].push(function () {
                                        Ya.Context.AdvManager.render({
                                            blockId: "R-A-423943-2",
                                            renderTo: "yandex_rtb_R-A-423943-2",
                                            async: true
                                        });
                                    });
                                    t = d.getElementsByTagName("script")[0];
                                    s = d.createElement("script");
                                    s.type = "text/javascript";
                                    s.src = "//an.yandex.ru/system/context.js";
                                    s.async = true;
                                    t.parentNode.insertBefore(s, t);
                                })(this, this.document, "yandexContextAsyncCallbacks");
                            </script>
                        </div>
                    <? endif; ?>
                </div>


                <div class="center_blocks main">


                    <div id="content">
                        <div id="c_left">
                                
 


                                            
  