<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arResult["~DETAIL_TEXT"] = str_replace('\"', '"', $arResult["~DETAIL_TEXT"]);
$arResult["NAME"] = str_replace('\"', '"', $arResult["~NAME"]);

$arResult["NAME"] = str_replace('\«', '«', $arResult["NAME"]);
$arResult["NAME"] = str_replace('\»', '»', $arResult["NAME"]);

if ($arResult["DETAIL_PICTURE"]["WIDTH"] > 350) $arResult["DETAIL_PICTURE"]["WIDTH"] = 350;

?>
<div class="news-detail">
    <?
    if (array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y") {
        ?>
        <div class="news-detail-share">
            <noindex>
                <?
                $APPLICATION->IncludeComponent(
                    "star:yashare",
                    ".default",
                    Array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "SERVISE_LIST" => array(0 => "vkontakte", 1 => "facebook", 2 => "odnoklassniki", 3 => "twitter", 4 => "lj", 5 => "viber", 6 => "whatsapp", 7 => "telegram",),
                        "TEXT_ALIGN" => "ar_al_left",
                        "TEXT_BEFORE" => "",
                        "VISUAL_STYLE" => "icons"
                    )
                );
                ?>
            </noindex>
        </div>
        <?
    }
    ?>
    <div class='img' style="float:left;">
        <? if ($arParams["DISPLAY_PICTURE"] != "N" && is_array($arResult["DETAIL_PICTURE"])):
            $file = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array('width' => 400, 'height' => 400), BX_RESIZE_IMAGE_PROPORTIONAL, false);
            $this->SetViewTarget('og:image');
            echo '<meta property="og:image" content="https://' . SITE_SERVER_NAME . $file["src"] . '"/>';
            $this->EndViewTarget();
            ?>
            <img class="detail_picture"
                 style="padding:5px;align-items: flex-end; display: flex;"
                 border="0"
                 src="<?= $file["src"] ?>"
                 alt="<?= $arResult["DETAIL_PICTURE"]["ALT"] ?>"
                 title="<?= $arResult["DETAIL_PICTURE"]["TITLE"] ?>"
            />
        <? endif ?>

        <? if ($arResult['PROPERTIES']['ICTOK']['VALUE'] != '') { ?>    <p class="textp"
                                                                           style="width:<?= $arResult["DETAIL_PICTURE"]["WIDTH"] ?>px; position: absolute; padding-left:10px; margin-top:-20px; font-size:8px;"><?= $arResult['PROPERTIES']['ICTOK']['VALUE']; ?><?= $arResult['PROPERTIES']['ICTOK']['DESCRIPTION']; ?></p> <? } ?>

    </div>


    <?


    if ($arResult['PROPERTIES']['AUTHORS']['VALUE'] > 0) {

        $resU = CIBlockElement::GetByID($arResult['PROPERTIES']['AUTHORS']['VALUE']);
        $ar_resU = $resU->GetNext();

        ?>         <span>Автор: </span><a href="/avtor/<?= $ar_resU['ID']; ?>/"><?= $ar_resU['NAME']; ?></a> <br><? } ?>



    <? if ($arParams["DISPLAY_DATE"] != "N" && $arResult["DISPLAY_ACTIVE_FROM"]): ?>
        <span class="news-date-time"><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></span>
    <? endif; ?>
    <? if ($arParams["DISPLAY_NAME"] != "N" && $arResult["NAME"]): ?>

    <? endif; ?>
    <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arResult["FIELDS"]["PREVIEW_TEXT"]): ?>
        <p><?= $arResult["FIELDS"]["PREVIEW_TEXT"];
            unset($arResult["FIELDS"]["PREVIEW_TEXT"]); ?></p>
    <? endif; ?>
    <? if ($arResult["NAV_RESULT"]): ?>
        <? if ($arParams["DISPLAY_TOP_PAGER"]): ?><?= $arResult["NAV_STRING"] ?><br/><? endif; ?>
        <? echo $arResult["NAV_TEXT"]; ?>
        <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?><br/><?= $arResult["NAV_STRING"] ?><? endif; ?>
    <? elseif (strlen($arResult["DETAIL_TEXT"]) > 0): ?>
        <?
        //$arResult["~DETAIL_TEXT"] = str_replace('<a href=', '<a rel="nofollow" href=', $arResult["~DETAIL_TEXT"]);
        $pos = 5000;
        $len = mb_strlen($arResult["~DETAIL_TEXT"], 'UTF-8');
        $count = round($len / $pos, 0);
        if (($len - $count * $pos) < ($pos / 2))
            $count--;
        if ($count > 0) {
            while ($count) {
                if ($replace_pos = mb_stripos($arResult["~DETAIL_TEXT"], '</p>', $pos * $count, 'UTF-8')) {
                    $part1 = mb_substr($arResult["~DETAIL_TEXT"], 0, $replace_pos - 1, 'UTF-8');
                    $part2 = mb_substr($arResult["~DETAIL_TEXT"], $replace_pos, null, 'UTF-8');
                    $arResult["~DETAIL_TEXT"] = $part1 . '<div id="yandex_rtb_R-A-423943-1-' . $count . '"></div>
                    <script type="text/javascript">
                        (function(w, d, n, s, t) {
                            w[n] = w[n] || [];
                            w[n].push(function() {
                                Ya.Context.AdvManager.render({
                                    blockId: "R-A-423943-1",
                                    renderTo: "yandex_rtb_R-A-423943-1-' . $count . '",
                                    async: true,
                                    pageNumber: ' . $count . '
                                });
                            });
                            t = d.getElementsByTagName("script")[0];
                            s = d.createElement("script");
                            s.type = "text/javascript";
                            s.src = "//an.yandex.ru/system/context.js";
                            s.async = true;
                            t.parentNode.insertBefore(s, t);
                        })(this, this.document, "yandexContextAsyncCallbacks");
                    </script>' . $part2;
                }
                $count--;
            }
        }
        ?>
        <? echo $arResult["~DETAIL_TEXT"]; ?>
    <? else: ?>
        <? echo $arResult["~PREVIEW_TEXT"]; ?>
    <? endif ?>
    <div style="clear:both"></div>
    <br/>
    <? foreach ($arResult["FIELDS"] as $code => $value):
        if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code) {
            ?><?= GetMessage("IBLOCK_FIELD_" . $code) ?>:&nbsp;<?
            if (!empty($value) && is_array($value)) {
                ?><img border="0" src="<?= $value["SRC"] ?>" width="<?= $value["WIDTH"] ?>"
                       height="<?= $value["HEIGHT"] ?>"><?
            }
        } else {
            ?><?= GetMessage("IBLOCK_FIELD_" . $code) ?>:&nbsp;<?= $value; ?><?
        }
        ?><br/>
    <?endforeach;
    foreach ($arResult["DISPLAY_PROPERTIES"] as $pid => $arProperty):?>

        <?= $arProperty["NAME"] ?>:&nbsp;
        <? if (is_array($arProperty["DISPLAY_VALUE"])):?>
            <?= implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]); ?>
        <?
        else:?>
            <?= $arProperty["DISPLAY_VALUE"]; ?>
        <?endif ?>
        <br/>
    <?endforeach;
    if (array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y") {
        ?>
        <div class="news-detail-share">
            <noindex>
                <?
                $APPLICATION->IncludeComponent(
                    "star:yashare",
                    ".default",
                    Array(
                        "COMPONENT_TEMPLATE" => ".default",
                        "SERVISE_LIST" => array(0 => "vkontakte", 1 => "facebook", 2 => "odnoklassniki", 3 => "twitter", 4 => "lj", 5 => "viber", 6 => "whatsapp", 7 => "telegram",),
                        "TEXT_ALIGN" => "ar_al_left",
                        "TEXT_BEFORE" => "",
                        "VISUAL_STYLE" => "icons"
                    )
                );
                ?>
            </noindex>
        </div>
        <?
    }
    ?>
</div>


<?
// topnewnum 

global $arFilter;

$APPLICATION->IncludeComponent("6dev:comm", ".default", Array(
        "ID" => $arResult['ID'],
        "IBLOCK_ID" => $arResult['IBLOCK_ID'],
        "NAME" => $arResult['NAME']
    )
); ?>

<script>
    var dd = $('.textp').height();
    $('.detail_picture').css('padding-bottom', dd + 'px');
</script>