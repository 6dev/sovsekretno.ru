<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?
foreach ($arResult["ITEMS"] as $arItem):?>

    <div class="main-post floatcontainer">
        <div class="tophead_post">
            <div class="left">
                <p><b>СВЕЖИЙ НОМЕР</b>: <?= $arItem['NAME']; ?></p> <!-- remove here 4/2019 -->
            </div>
            <div class="right">
                <a href="<?= $arItem['DETAIL_PAGE_URL']; ?>" class="listen_number"><span class="icon">
					<img src="/public/site/img/new_images/icons/icon_1.png" alt="icon"/></span><span class="text">ЧИТАТЬ<br/>НОМЕР</span></a>
                <a href="/magazines/" class="archive_numbers"><span class="icon">
					<img src="/public/site/img/new_images/icons/icon_2.png" alt="icon"/></span><span
                            class="text">АРХИВ<br/>НОМЕРОВ</span></a>
            </div>
            <div class="clear"></div>
        </div>
        <div class="left">

            <div class="img">
                <a href="<?= $arItem['DETAIL_PAGE_URL']; ?>">
                    <? $file = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"], array('width' => 215, 'height' => 312)); ?>
                    <img style="margin: -10px 0 0;" src="<?= $file['src']; ?>" alt="Свежий номер" class="num_pic">
                </a>
            </div>
        </div>

        <? // Запросить	страницы 5 шт
        ?>
        <?
        $i = 0;
        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_AUTHORS", "DETAIL_PAGE_URL", "DETAIL_PICTURE", 'PREVIEW_TEXT', 'PROPERTY_MAIN', "SHOW_COUNTER");
        //$arFilter = Array("IBLOCK_ID"=>3, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y",'PROPERTY_NUMBERS'=>$arItem['ID'], 'PROPERTY_MAIN'=>'Y');
        $arFilter = Array("IBLOCK_ID" => 3, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", 'PROPERTY_MAIN_VALUE' => 'Y');
        $res = CIBlockElement::GetList(Array("timestamp_x" => "desc"), $arFilter, false, Array("nPageSize" => 5), $arSelect);
        while ($ob = $res->GetNextElement()) {
            $i = $i + 1;
            $arFields = $ob->GetFields();

            $arFields["NAME"] = str_replace('\"', '"', $arFields["NAME"]);

            $arFields["NAME"] = str_replace('\«', '«', $arFields["NAME"]);

            $arFields["NAME"] = str_replace('\»', '»', $arFields["NAME"]);

            ?>

            <div class="right">
                <div class="edit_column"> <!-- box_news box_news_editorial -->
                    <h3><a href="<?= $arFields['DETAIL_PAGE_URL']; ?>" class="title"><?= $arFields['NAME']; ?></a></h3>

                    <?


                    if ($arFields['PROPERTY_AUTHORS_VALUE'] > 0) {

                        $resU = CIBlockElement::GetByID($arFields['PROPERTY_AUTHORS_VALUE']);
                        $ar_resU = $resU->GetNext();

                        ?>         <span>Автор: </span><a
                                href="/avtor/<?= $ar_resU['ID']; ?>/"><?= $ar_resU['NAME']; ?></a> <br><? } ?>


                    <div class="box">
                        <a href="<?= $arFields['DETAIL_PAGE_URL']; ?>">
                            <? if ($arFields['DETAIL_PICTURE'] > 0) {

                            $image = CFile::ResizeImageGet($arFields['DETAIL_PICTURE'], Array("width" => 210, "height" => 151), BX_RESIZE_IMAGE_EXACT, false);


                            $image_big = CFile::ResizeImageGet($arFields['DETAIL_PICTURE'], Array("width" => 450, "height" => 450), BX_RESIZE_IMAGE_PROPORTIONAL, false);
                            echo '<img src="' . $image['src'] . '"
                            srcset="' . $image['src'] . ' 210w, ' . $image_big['src'] . ' 450w" 
                            sizes="(max-width: 767px) 450px, (min-width: 767px) 210px"
                            alt="' . $arFields['NAME'] . '"
                            title="' . $arFields['NAME'] . '">';
                            ?>
                        </a> <? } ?>
                    </div>
                    <div><a href="<?= $arFields['DETAIL_PAGE_URL']; ?>" style="color: #000000">
                    <strong><?= strip_tags($arFields["~PREVIEW_TEXT"]); ?></strong></a></div> <!-- edit here -->
                    <div class="floatcontainer post_controls">
                        <div class="viewings"><?=$arFields['SHOW_COUNTER'];?></div>
                    </div>
                </div>
            </div>


            <?

            if ($i == 2) echo '  <div style="clear: both;"></div>';


        } ?>


    </div>

<? endforeach; ?>
