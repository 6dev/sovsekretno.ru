<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?foreach($arResult["ITEMS"] as $arItem):?>

	<div class="box_article">
                        <div class="item first">
                            <div class="inner">
                                <div class="innerinner">
                                                                                                                <div class="img">
                                            <a href="<?=$arItem['DETAIL_PAGE_URL'];?>">
                                                <? if ($arItem['PREVIEW_PICTURE']>0) {
                                                    $image = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], Array("width" => 210, "height" => 151), BX_RESIZE_IMAGE_EXACT, false);
                                                    $image_big = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], Array("width" => 450, "height" => 450), BX_RESIZE_IMAGE_PROPORTIONAL, false);
                                                    echo '<img 
    src="' . $image['src'] . '"
    srcset="' . $image['src'] . ' 210w, ' . $image_big['src'] . ' 450w" 
    sizes="(max-width: 767px) 450px, (min-width: 767px) 210px"
    alt="' . $arItem['NAME'] . '"
    title="' . $arItem['NAME'] . '">';
                                                }
                                                ?>

                                            </a>
                                        </div>
                                                                        <div class="title"><h3><a href="<?=$arItem['DETAIL_PAGE_URL'];?>"><?=$arItem['NAME'];?></a></h3></div>
                                    <div class="text"><a href="<?=$arItem['DETAIL_PAGE_URL'];?>"><p>
	<strong><?=$arItem["~PREVIEW_TEXT"];?></strong></p></a></div>
                                    <div class="floatcontainer post_controls">
                                        <div class="comments" data-xid="articles_6227">0</div>
                                        <div class="viewings"><?=$arItem['SHOW_COUNTER'];?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

<?endforeach;?>
