<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<? foreach ($arResult["ITEMS"] as $arItem): ?>

    <div class="box_article box_press">
        <div class="item first">
            <div class="inner">
                <div class="innerinner">
                    <div class="title">
                        <h3><a href="<?= $arItem['DETAIL_PAGE_URL']; ?>"><?= $arItem['NAME']; ?></a></h3>
                    </div>
                    <div class="text">
                        <a href="<?= $arItem['DETAIL_PAGE_URL']; ?>">
                            <p><strong><?= $arItem["~PREVIEW_TEXT"]; ?></strong></p>
                        </a>
                    </div>
                    <div class="floatcontainer post_controls">
                        <div class="viewings"><?= $arItem['SHOW_COUNTER']; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? endforeach; ?>
