<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


   <div class="center_blocks">
   
   
   
  <div class="paging floatcontainer">

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=str_replace('onclick','onclickno',$arResult["NAV_STRING"])?>
<?endif;?>

</div>  
   
   
   
   

                    <div class="articles_list news_list main-post" id="c_left">

<?foreach($arResult["ITEMS"] as $arItem):?>


	<div class="box_news">
			<div class="left">
				<div class="img">
					<img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" />
				</div>
			</div>
			<div class="right">
				<a href="<?=$arItem['DETAIL_PAGE_URL'];?>" class="title"><?=$arItem['NAME'];?></a>
                                                    
				<div class="date"><?=$arItem['ACTIVE_FROM'];?></div>
				<p class="preview_text"><?=$arItem["~PREVIEW_TEXT"];?></p>
                <div class="floatcontainer post_controls">
                    <div class="viewings"></div>
                </div>
			</div>
	</div>











<?endforeach;?>


  <div class="paging floatcontainer">

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><? str_replace('onclick','onclickno',$arResult["NAV_STRING"])?>
<?endif;?>

</div>  
   


<br><br><br>

</div>
</div>

