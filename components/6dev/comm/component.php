<div id="commentsList" style="overflow: hidden;">

    <script>
        $(document).ready(function () {
            //var prettyprint = false;
            $("pre").each(function () {
                //alert($(this).attr('class'));
                if ($(this).attr('class') == undefined) {
                    $(this).addClass('prettyprint linenums');
                }
            });
            prettyPrint();

            $('.comment-item-content-hide-part').readmore({
                speed: 75,
                collapsedHeight: 150,
                moreLink: '<p class="text-right pdn-right-15 mrgn-top-10 readmore-more"><small><a href="#">Развернуть комментарий</a></small></p>',
                lessLink: '<p class="text-right pdn-right-15 mrgn-top-10 readmore-more"><small><a href="#">Свернуть комментарий</a></small></p>',
            });
        });
    </script>
    <br><h4>Комментарии</h4>
    <hr>

    <ul class="media-list">

        <?
        CModule::IncludeModule('iblock');

        $m = array('нулябрь', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');

        $N = array('понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье');

        $arSelect = Array("ID", "DETAIL_TEXT", "IBLOCK_ID", "ACTIVE_FROM", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_NAME");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
        $arFilter = Array("IBLOCK_ID" => 8, "PROPERTY_OBJ" => $arParams['ID'], "ACTIVE" => "Y");


        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 50), $arSelect);
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();

            $dt = strtotime($arFields['ACTIVE_FROM']);

            $dm = $m[date('n', $dt)];
            $dN = $N[date('N', $dt)];
            $dd = date('d', $dt);

            $d = $dN . ', ' . $dd . ' ' . $dm . ' ' . date('Y в H:i:s', time());

            ?>

            <li id="node_<?= $arFields['ID']; ?>" rel="Zipper" class="media">
                <div class="media-left">
                </div>
                <div class="media-body2 popoverMediaBody2" style="width:98%">
                    <div class="popover3 right show commentPopover block-shadow">
                        <div class="arrow commentPopoverArrow"></div>
                        <div class="popover-title commentPopoverTitle">&nbsp;<strong class="pull-left"
                                                                                     style="overflow: hidden;"><?= $arFields['PROPERTY_NAME_VALUE']; ?></strong>
                            <span class="pull-right">
<small class="help-block"><?= $d; ?><a name="<?= $arFields['ID']; ?>" title="Постоянная ссылка на комментарий"
                                       href="#<?= $arFields['ID']; ?>"> #<?= $arFields['ID']; ?></a>
</small></span></div>
                        <div class="popover-content comment-item-content ">
                            <p class=""><?= $arFields['DETAIL_TEXT']; ?></p></div>
                        <div class="popover-content comment-item-content ">
                            <div id="answerForm-986"></div>
                            <p style="margin:0;">
                                <small>
                                    <a onclick="showAnswerForm(986); return false;"
                                       id="answerLink-<?= $arFields['ID']; ?>" class="answerLink" href="#"></a></small>
                            </p>
                        </div>
                    </div>
                    <br>
                </div>
            </li>


        <? } ?>

    </ul>
</div>


<div id="commentForm">
    <div id="commentFormPreload"></div>

    <script src="https://www.google.com/recaptcha/api.js?render=6LdBI8cUAAAAACqusyAWdoWo8GoegGC0nvXSrSOF"></script>
    <script>
        grecaptcha.ready(function () {
            grecaptcha.execute('6LdBI8cUAAAAACqusyAWdoWo8GoegGC0nvXSrSOF', { action: 'comment' }).then(function (token) {
                var recaptchaResponse = document.getElementById('recaptchaResponse');
                recaptchaResponse.value = token;
            });
        });
    </script>

    <form class="contact-form" role="form" id="uComment" method="POST">
        <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
        <hr>
        <h4 id="commentFormHeader">Оставить комментарий</h4><input value="67" name="Comments[postsId]"
                                                                   id="Comments_postsId" type="hidden"><input value="0"
                                                                                                              name="Comments[pid]"
                                                                                                              id="Comments_pid"
                                                                                                              type="hidden"><input
                value="0" name="Comments[level]" id="Comments_level" type="hidden">
        <div class="row" id="comment-auth">

            <input type='hidden' name='ADD_ID' value='<?= $arParams['ID']; ?>'>


            <div class="col-sm-4">
                <p>
                    <small>Войдите через социальную сеть</small>
                </p>

                <?php
                $APPLICATION->IncludeComponent(
                    "ulogin:auth",
                    "",
                    Array(
                        "SEND_MAIL" => "N",
                        "SOCIAL_LINK" => "Y",
                        "GROUP_ID" => array("6"),
                        "ULOGINID1" => "",
                        "ULOGINID2" => ""
                    )
                );

                global $USER;

                $rsUser = CUser::GetByID($USER->GetID());
                $arUser = $rsUser->Fetch();

                ?>

            </div>

            <div class="col-sm-8">
                <p>
                    <small>или заполните следующие поля</small>
                </p>
                <div class="form-group">
                    <input value="<?= $arUser['NAME']; ?>" class="form-control comment-input"
                           placeholder="Введите Ваше Имя" name="Comments[name]" id="Comments_name" type="text"
                           maxlength="255">
                    <div class="small text-danger" id="Comments_name_em_" style="display:none"></div>
                </div>
                <div class="form-group">
                    <input value="<?= $arUser['EMAIL']; ?>" class="form-control comment-input"
                           placeholder="Введите Вашу э-почту" name="Comments[email]" id="Comments_email" type="text"
                           maxlength="255">
                    <div class="small text-danger" id="Comments_email_em_" style="display:none"></div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="wysibb">
                <div class="wysibb-toolbar" style="max-height: 39px;">


                </div>
                <div class="wysibb-text">
<textarea placeholder="Введите Ваш комментарий" style="width:100%" id="Comments_text" name="Comments[text]"
          class="wysibb-texarea">
</textarea>

                    <div class="wysibb-text-editor wysibb-body"
                         onblur="if($(this).text() == '') { $('#Comments_text').htmlcode(''); } $('.wysibb').removeClass('wysibb-focus wysibb-has-error-focus '); "
                         data-text="Введите Ваш комментарий" style="max-height:800px;min-height:39px"
                         contenteditable="true"></div>
                </div>
                <div class="bottom-resize-line"></div>
            </div>
            <div class="small text-danger" id="Comments_text_em_" style="display:none"></div>
        </div>
        <div class="text-right" style="text-align:left!important;"><span class="text-left pull-left"><input
                        id="ytComments_notify" type="hidden" value="0" name="Comments[notify]"><input
                        class="comment-input" name="Comments[notify]" id="Comments_notify" value="1" type="checkbox">
<label style="padding-right:30px;" class="ch-box-label comment-label" for="Comments_notify">Получить уведомление о новых комментариях к статье по почте </label> </span>
            <button type="reset" onclick="resetBtn(); return false;" class="btn comment-btn" name="yt1">Отмена</button>
            &nbsp;
            <button style="color:#000000;" type="submit" class="btn btn-success alt-right comment-btn comment-send-btn">
                Отправить
            </button>
        </div>
    </form>
</div>


<style>
    #commentForm {
        overflow: hidden;
        float: left;
        width: 100%;
    }

    #commentFormPreload {
        /* background-color: #777; */
        position: absolute;
        z-index: 99999;
        background-color: #f5f4f4;
        opacity: 0.2;
        display: none;
        /* width: 100%; */
    }

    .commentPopover {
        position: relative;
        width: 100%;
        max-width: 100%;
        z-index: 0;
        margin-left: 0px !important;
    }

    .commentPopoverArrow {
        top: 18px !important;
    }

    .popoverMediaBody {
        width: 100%;
        padding-left: 80px;
    }

    ul.media-list li {
        width: 100%;
    }

    #commentsList .media-left {
        position: absolute;
        width: 74px;
    }

    #commentsList .media-list li {
        float: left;
        clear: both;
    }

    #commentsList .media-left, .media-body {
        float: left;
        display: block;
    }

    .footer-blockquote {
        float: left;
        width: 100%;
    }

    .commentPopoverTitle {
        padding: 8px 14px;
    }

    .popover {
        position: absolute;
        top: 0;
        left: 0;
        z-index: 1060;
        display: none;
        max-width: 276px;
        padding: 1px;
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: left;
        white-space: normal;
        background-color: #fff;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        border: 1px solid #ccc;
        border: 1px solid rgba(0, 0, 0, .2);
        border-radius: 6px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, .2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, .2)
    }

    .popover.top {
        margin-top: -10px
    }

    .popover.right {
        margin-left: 10px
    }

    .popover.bottom {
        margin-top: 10px
    }

    .popover.left {
        margin-left: -10px
    }

    .popover-title {
        padding: 8px 14px;
        margin: 0;
        font-size: 14px;
        background-color: #f7f7f7;
        border-bottom: 1px solid #ebebeb;
        border-radius: 5px 5px 0 0
    }

    .popover-content {
        padding: 9px 14px
    }

    .popover > .arrow, .popover > .arrow:after {
        position: absolute;
        display: block;
        width: 0;
        height: 0;
        border-color: transparent;
        border-style: solid
    }

    .popover > .arrow {
        border-width: 11px
    }

    .popover > .arrow:after {
        content: "";
        border-width: 10px
    }

    .popover.top > .arrow {
        bottom: -11px;
        left: 50%;
        margin-left: -11px;
        border-top-color: #999;
        border-top-color: rgba(0, 0, 0, .25);
        border-bottom-width: 0
    }

    .popover.top > .arrow:after {
        bottom: 1px;
        margin-left: -10px;
        content: " ";
        border-top-color: #fff;
        border-bottom-width: 0
    }

    .popover.right > .arrow {
        top: 50%;
        left: -11px;
        margin-top: -11px;
        border-right-color: #999;
        border-right-color: rgba(0, 0, 0, .25);
        border-left-width: 0
    }

    .popover.right > .arrow:after {
        bottom: -10px;
        left: 1px;
        content: " ";
        border-right-color: #fff;
        border-left-width: 0
    }

    .popover.bottom > .arrow {
        top: -11px;
        left: 50%;
        margin-left: -11px;
        border-top-width: 0;
        border-bottom-color: #999;
        border-bottom-color: rgba(0, 0, 0, .25)
    }

    .popover.bottom > .arrow:after {
        top: 1px;
        margin-left: -10px;
        content: " ";
        border-top-width: 0;
        border-bottom-color: #fff
    }

    .popover.left > .arrow {
        top: 50%;
        right: -11px;
        margin-top: -11px;
        border-right-width: 0;
        border-left-color: #999;
        border-left-color: rgba(0, 0, 0, .25)
    }

    .popover.left > .arrow:after {
        right: 1px;
        bottom: -10px;
        content: " ";
        border-right-width: 0;
        border-left-color: #fff
    }

    .media-body {
        -ms-flex: 1;
        flex: 1;
    }

    .show {
        display: block !important;
    }

    .block-shadow {
        -webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, .1);
        box-shadow: 0 2px 5px rgba(0, 0, 0, .1);
    }

</style>


<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH; ?>/css/bootstrap.min.css"
      integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<script src="<?= SITE_TEMPLATE_PATH; ?>/css/readmore.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>


<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
      integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">


<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>


<? foreach ($arResult["ITEMS"] as $arItem): ?>


<? endforeach; ?>
